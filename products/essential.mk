#
# Copyright (C) 2023 The BlissRoms Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Essential Packages
# Carrier Services
PRODUCT_PACKAGES += \
    CarrierServices \

# Calculator
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \

# Clock
PRODUCT_PACKAGES += \
    PrebuiltDeskClockGoogle \
    GoogleClockOverlay \

# Contacts
PRODUCT_PACKAGES += \
    GoogleContacts \
    GoogleContactsOverlay \

# Dialer
PRODUCT_PACKAGES += \
    GoogleDialer \
    GoogleDialerOverlay \

# Messaging
PRODUCT_PACKAGES += \
    PrebuiltBugle \
    GoogleMessagesOverlay

# Digital Wellbeing
PRODUCT_PACKAGES += \
    WellbeingPrebuilt \
    DigitalWellbeingOverlay \

# Drive
PRODUCT_PACKAGES += \
    Drive \

# Maps
PRODUCT_PACKAGES += \
    LocationHistoryPrebuilt \
    Maps \
    GoogleLocationHistoryOverlay \

# Photos
PRODUCT_PACKAGES += \
    Photos \
    GooglePhotosOverlay \

# Search
PRODUCT_PACKAGES += \
    Velvet \
    VelvetOverlay

# Calendar
PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt \

# Files
PRODUCT_PACKAGES += \
    DocumentsUIGoogle \
    FilesPrebuilt \
    StorageManagerGoogle \
    PixelDocumentsUIGoogleOverlay \

# GBoard (LatinIME)
PRODUCT_PACKAGES += \
    LatinIMEGooglePrebuilt \

PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.ime.bs_theme=true \
    ro.com.google.ime.theme_id=5 \
    ro.com.google.ime.system_lm_dir=/product/usr/share/ime/google/d3_lms

# SoundPicker
PRODUCT_PACKAGES += \
    SoundPickerPrebuilt \

# Chrome Browser
PRODUCT_PACKAGES += \
    Chrome-Stub \
    TrichromeLibrary-Stub \
    WebViewGoogle-Stub \
    ScribePrebuilt \

# SetupWizard
PRODUCT_PACKAGES += \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt \
    PartnerSetupPrebuilt \
    SetupWizardPrebuilt \

# Turbo
PRODUCT_PACKAGES += \
    TurboPrebuilt \
    DeviceHealthServicesOverlay \
    GMSConfigSettingsOverlayTurbo \

# SetupWizard Props
PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.rotation_locked=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.enable_assist_gesture_training=true \
    setupwizard.theme=glif_v3_light \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=false

