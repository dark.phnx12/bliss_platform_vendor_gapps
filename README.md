This project aims to bring Gapps support to AOSP builds, directly extracted from Pixel devices.

The project aims at the usage of variants for the size of Gapps that are to be included.
```
TARGET_ESSENTIAL_GAPPS := true
TARGET_STOCK_GAPPS := true
```

This repository was created for the use of GMS in Bliss and other projects:


GMS Default packages:
```
ConfigUpdater 
AndroidPlatformServices 
GoogleContactsSyncAdapter 
GoogleCalendarSyncAdapter 
GoogleServicesFramework 
libjni_latinimegoogle 
MlkitBarcodeUIPrebuilt 
Phonesky 
PrebuiltGmsCoreSc 
PrebuiltGmsCoreSc_AdsDynamite 
PrebuiltGmsCoreSc_CronetDynamite 
PrebuiltGmsCoreSc_DynamiteLoader 
PrebuiltGmsCoreSc_DynamiteModulesA 
PrebuiltGmsCoreSc_DynamiteModulesC 
PrebuiltGmsCoreSc_GoogleCertificates 
PrebuiltGmsCoreSc_MapsDynamite 
PrebuiltGmsCoreSc_MeasurementDynamite 
VisionBarcodePrebuilt 
com.google.android.dialer.support
```

GMS Essential packages
```
Carrier Services
Calculator
Clock
Contacts
Dialer
Messaging
Digital Wellbeing
Drive
Maps
Photos
Search
SetupWizard
Turbo
```

GMS Stock packages included on top of Stock packages
```
Android Auto
Calendar
Device Policy
Files
Gmail
GBoard
Google TTS
Markup
Recorder
SoundPicker
```
